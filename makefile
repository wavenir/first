all: main.o myfunc.o
	gcc -o one main.o myfunc.o

main.o: main.c header.h declarations.h
	gcc -c main.c

myfunc.o:myfunc.o header.h declarations.h
	gcc -c myfunc.c
clean:
	rm -rf *.o one
